import React, { Component } from 'react';
import './App.css';
import axios from 'axios';


import logohome from './assets/Imagens/logo-home.png';
import icemail from './assets/Imagens/ic-email.svg';
import iccadeado from './assets/Imagens/ic-cadeado.svg';


// var auth = [];


class App extends Component {


  constructor(props) {
    super(props)
    this.state = {
      username: 'testeapple@ioasys.com.br', password: '12341234',
      status: ''
    }

    this.tratarerro = this.tratarerro.bind(this);
    this.login = this.login.bind(this);

  }

  tratarerro(error){
    this.setState({ status: error.message });
  }

  login() {
    this.setState({ status: 'CARREGANDO' });

    var authOptions = {
      method: 'POST',
      url: 'http://54.94.179.135:8090/api/v1/users/auth/sign_in',
      //data: qs.stringify(data),
      data: { 'email': this.state.username, 'password': this.state.password },
      /* headers: {
         'Authorization': 'Basic Y2xpZW50OnNlY3JldA==',
         'Content-Type': 'application/x-www-form-urlencoded'
       },*/
      json: true
    };


    axios(authOptions).then(response => {
      // auth = response.headers;
      // console.log(auth);

      localStorage.setItem('access-token', response.headers['access-token']);
      localStorage.setItem('uid', response.headers['uid']);
      localStorage.setItem('client', response.headers['client']);

      this.setState({ username: response.headers.uid });
      this.setState({ password: '' });

      this.props.history.push("/enterprises");
      /*this.props.history.push({
        pathname: '/enterprises',
        search: '?query=abc',
        state: { detail: 'teste' }
      });*/
    }).catch(function (error) {
      console.log(error);
      this.tratarerro(error);
    }.bind(this));;;
  }


  handleChangeUsername(e) {
    this.setState({ username: e.target.value });
  }

  handleChangePassword(e) {
    this.setState({ password: e.target.value });
  }


  render() {
    return (
      <div className="justify-content-sm-center align-items-center justify-content-center text-center">
        <div className="conteudo">
          <article className="logo logohomee" align='center'>
            <img src={logohome} alt="IMG" />
          </article>
          <article className="saudacoes justify-content-sm-center">
            <p className="BEMVINDO col-4 offset-sm-4">BEM-VINDO AO EMPRESAS</p>
            <p className="lo Lorem col-10 offset-sm-1" style={{ textAlign: 'center' }}>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
            <p className="lo Lorem col-10 offset-sm-1" style={{ textAlign: 'center', color : 'red'}}>{this.state.status}</p>
          </article>

          <div className="form-group input-group col-8 offset-md-2">
            <span className="input-group-addon" id="basic-addon1"><img src={icemail} alt="IMG" /></span>
            <input style={{ height: '50px' }} type="email" name="email_text" value={this.state.username} onChange={this.handleChangeUsername.bind(this)} placeholder="Email" className="form-control" />
          </div>

          <div className="form-group input-group col-8 offset-md-2">
            <span className="input-group-addon" id="basic-addon2"><img src={iccadeado} alt="IMG" /></span>
            <input style={{ height: '50px' }} name="senha_text" type="password" value={this.state.password} onChange={this.handleChangePassword.bind(this)} placeholder="Senha" className="form-control" />
          </div>

          <article align='center'>
            <button onClick={this.login} className="btn btn-entrar botaoentrar" style={{color: 'white'}}>ENTRAR</button>
          </article>

        </div>
      </div>
    );
  }





  /*
render() {
return (
  <div className='button__container'>
      <button className='button' onClick={this.login}>
        Click Me First
    </button>
      <p>{this.state.username}</p>
      <button className='button' onClick={this.getEnterprises}>
        Click Me Then
  </button>
    </div>

    )
  }
  */

  /*
render() {
return (
  <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to React</h1>
      </header>
      <p className="App-intro">
        To get started, edit <code>src/App.js</code> and save to reload.
    </p>
    </div>
    );
  }*/


}

export default App;
