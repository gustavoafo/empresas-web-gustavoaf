import React, { Component } from 'react';

import axios from 'axios';

import './Enterprises.css';

import imglista from '../assets/Imagens/img-e-1-lista.svg';
import icsearchcopy from '../assets/Imagens/ic-search-copy-2.svg';
import icclose from '../assets/Imagens/ic-close.svg';

import { debounce } from 'throttle-debounce';

class Enterprises extends Component {


    constructor(props) {
        super(props)
        this.state = {
            'enterprises': [],
            'busca': '',
            'buscaTipo': 0
        }

        this.searchbyname = this.searchbyname.bind(this);
        this.searchbyType = this.searchbyType.bind(this);
        this.searchbynameAndType = this.searchbynameAndType.bind(this);
        this.handleBusca = this.handleBusca.bind(this);

        //console.log(props);
        //console.log(props.location.state.detail);

    }

    componentWillMount() {

        var config = {
            headers: {
                'Content-Type': 'application/json',
                'access-token': localStorage.getItem('access-token'),
                'client': localStorage.getItem('client'),
                'uid': localStorage.getItem('uid')
            }
        };


        axios.get('http://54.94.179.135:8090/api/v1/enterprises', config).then(response => {
            this.setState({ 'enterprises': response.data.enterprises })

            //console.log(this.state.enterprises);
        }).catch(function (error) {
            console.log(error);
        });;
    }

    searchbyname() {
        var config = {
            headers: {
                'Content-Type': 'application/json',
                'access-token': localStorage.getItem('access-token'),
                'client': localStorage.getItem('client'),
                'uid': localStorage.getItem('uid')
            }
        };

        axios.get('http://54.94.179.135:8090/api/v1/enterprises?name=' + this.state.busca, config).then(response => {
            this.setState({ 'enterprises': response.data.enterprises })

            console.log(this.state.enterprises);
        }).catch(function (error) {
            console.log(error);
        });;
    }

    searchbyType() {
        var config = {
            headers: {
                'Content-Type': 'application/json',
                'access-token': localStorage.getItem('access-token'),
                'client': localStorage.getItem('client'),
                'uid': localStorage.getItem('uid')
            }
        };

        axios.get('http://54.94.179.135:8090/api/v1/enterprises?enterprise_types=' + this.state.buscaTipo, config).then(response => {
            this.setState({ 'enterprises': response.data.enterprises })

            console.log(this.state.enterprises);
        }).catch(function (error) {
            console.log(error);
        });;
    }

    searchbynameAndType() {
        var config = {
            headers: {
                'Content-Type': 'application/json',
                'access-token': localStorage.getItem('access-token'),
                'client': localStorage.getItem('client'),
                'uid': localStorage.getItem('uid')
            }
        };

        axios.get('http://54.94.179.135:8090/api/v1/enterprises?enterprise_types=' + this.state.buscaTipo + '&name=' + this.state.busca, config).then(response => {
            this.setState({ 'enterprises': response.data.enterprises })

            console.log(this.state.enterprises);
        }).catch(function (error) {
            console.log(error);
        });;
    }

    //setBusca = debounce((searchTerm) => this.setState({ busca: searchTerm }), 1000);
    setBusca = debounce(50, function (search) {
        this.setState({ busca: search });
        this.handleBusca();
    });

    setBuscaTipo = debounce(50, function (search) {
        this.setState({ buscaTipo: search });
        this.handleBusca();
    });

    handleChangeBusca(e) {
        this.setBusca(e.target.value);
        // this.setState({ busca: e.target.value });
        // this.searchbyname();
    }

    handleChangeBuscaTipo(e) {
        this.setBuscaTipo(e.target.value);
        //this.setState({ buscaTipo: e.target.value });
        //this.searchbyname();

        this.handleBusca();
    }

    handleBusca() {
        //console.log(this.state.busca);
        //console.log(this.state.buscaTipo);

        if (this.state.busca && this.state.buscaTipo != 0) {
            this.searchbynameAndType();
        }
        else if (this.state.busca && this.state.buscaTipo == 0) {
            this.searchbyname();
        }
        else if (!this.state.busca && this.state.buscaTipo != 0) {
            this.searchbyType();
        }
        else {
            this.componentWillMount();
        }
    }


    exibirDetalhes(any) {
        this.props.history.push({
            pathname: '/details',
            state: { detail: any }
        });
    }

    render() {

        let enterprises = this.state.enterprises;

        // <h4 key={item.id}> {item.description} </h4>

        return (

            <div style={{ backgroundColor: '#ebe9d7' }}>
                <div className="form-group input-group searchtopoo">
                    <div title="Click para buscar empresa" style={{ padding: '3px', cursor: 'pointer' }} className="input-group-addon mediumpink col-2"
                        id="basic-addon1"><img src={icsearchcopy} className="mediumpink" alt="" /></div>
                    <div className="d-flex align-items-center col-8">
                        <input type="text" name="nome_busca" value={this.state.busca} onChange={this.handleChangeBusca.bind(this)} autoFocus placeholder="Pesquise pelo nome ou selecione o tipo de empresa" className="form-control" />
                        <select name="tipo_select" style={{}} value={this.state.buscaTipo} onChange={this.handleChangeBuscaTipo.bind(this)} className="form-control">
                            <option value='0'>BUSQUE EMPRESA PELO TIPO</option>
                            <option value='1'>Agro</option>
                            <option value='2'>Aviation</option>
                            <option value='3'>Biotech</option>
                            <option value='4'>Eco</option>
                            <option value='5'>Ecommerce</option>
                            <option value='6'>Education</option>
                            <option value='7'>Fashion</option>
                            <option value='8'>Fintech</option>
                            <option value='9'>Food</option>
                            <option value='10'>Games</option>
                            <option value='11'>Health</option>
                            <option value='12'>IOT</option>
                            <option value='13'>Logistics</option>
                            <option value='14'>Media</option>
                            <option value='15'>Mining</option>
                            <option value='16'>Products</option>
                            <option value='17'>Real Estate</option>
                            <option value='18'>Service</option>
                            <option value='19'>Smart City</option>
                            <option value='20'>Social</option>
                            <option value='21'>Software</option>
                            <option value='22'>Technology</option>
                            <option value='23'>Tourism</option>
                            <option value='24'>Transport</option>
                        </select>

                    </div>


                    <span style={{ padding: '3px', cursor: 'pointer', border: 'none' }} className="input-group-addon mediumpink col-2" id="basic-addon1"><img src={icclose} className="mediumpink" alt="" /></span>

                    <div className="d-flex align-items-center col-8">

                    </div>
                </div>
                {enterprises.map(item =>
                    <div key={item.id} className="panel panel-default col-md-10 col-lg-10 col-sm-12 offset-md-1" style={{ cursor: 'pointer', background: '#fff', 'marginTop': '1%', 'borderRadius': '3px' }}>

                        <div onClick={() => { this.exibirDetalhes(item) }} className="panel-body" style={{ padding: '12px' }}>
                            <div className="row">
                                <div className="col-sm-12 col-md-3 img-company"><img src={imglista} alt="" style={{width: '100%'}}/></div>
                                <div className="col-sm-12 col-md-9">
                                    <ul>
                                        <li className="Empresa" style={{height:'100%'}}>{item.enterprise_name}</li>
                                        <li className="Negcio" >{item.enterprise_type.enterprise_type_name}</li>
                                        <li className="Pais" >{item.country}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                )
                }
            </div>
        )



    }



    /*
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src="" className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
            </p>
            </div>
        );
    }*/

}

export default Enterprises;