import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Route , Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';

import Enterprises from './enterprises/Enterprises';
import Details from './details/Details';


//ReactDOM.render(<App />, document.getElementById('root'));

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={App} />
            <Route path='/enterprises' component={Enterprises} />
            <Route path='/details' component={Details} />
        </Switch>
    </BrowserRouter> , document.getElementById('root')
);




registerServiceWorker();
