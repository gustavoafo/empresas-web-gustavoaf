import React, { Component } from 'react';

import axios from 'axios';

import './Details.css';

import imglista from '../assets/Imagens/img-e-1-lista.svg';

class Details extends Component {


    constructor(props) {
        super(props)
        this.state = {
            'detail': props.location.state.detail
        }
        console.log(this.state.detail);
    }

    render() {
        return (
            <div className="panel panel-default col-lg-8 col-md-8 offset-lg-2 col-sm-12" style={{ cursor: 'pointer', background: '#fff', 'marginTop': '1%', 'borderRadius': '3px' }}>

                <div className="panel-body" style={{ padding: '12px' }}>
                    <div className="row col-sm-12">
                        <div className="col-sm-12 img-company"><img src={imglista} alt="" className="col-sm-12"/></div>
                        <p className="col-sm-12 Lorem">
                            
                            {this.state.detail.description}
                            
                        </p>
                    </div>
                </div>

            </div>
        );
    }
}

export default Details;